# CSS Inliner Tests
This is just a personal work in progress to streamline the workflow for creating suites of html email templates in a simpler fashion.

### Goals
* To use actual stylesheets
* Import referenced stylesheets in templates
* Inline the css and ugly up the html

### Requirements
* Grunt

### Getting Started
1. [Install NPM](http://howtonode.org/introduction-to-npm)
2. [Install grunt](http://gruntjs.com/getting-started)
2. `cd` to your project folder
3. run `npm install`
4. run `grunt watch`
5. tweak away on your css & templates in the `build` folder

### Notes
* Do not directly edit any templates in the `dist` folder. These are your completed templates.

### Known Bugs
* inline css removes doctype [see issue](https://github.com/jgallen23/grunt-inline-css/issues/11 )