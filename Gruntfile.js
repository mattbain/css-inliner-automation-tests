module.exports = function(grunt) {

	// 1. All configuration goes here 
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		inline: {
			dist: {
				options:{
					cssmin: true
				},
				src: ['build/*.html'],
				dest: ['dist/']
			}
		},

		// KNOWN BUG: Doctype get's removed by inline css grunt contrib 
		// @link https://github.com/jgallen23/grunt-inline-css/issues/11 
		inlinecss: {
			main: {
				options: {
					removeStyleTags: false
				},
				files: {
					'dist/pw-march.html' : 'dist/pw-march.html'  // OUTFILE : INFILE
				}
			}
		},

		emailBuilder: {
			inline: {
				files: { 'dist/pw-march.html' : 'build/pw-march.html' },
				options: {
					encodeSpecialChars: true
				}
			}
		},

		watch: {
			templates: {
				files: 'build/*.html',
				tasks: ['inline', 'inlinecss'], //['emailBuilder:inline']
				options: {
					nospawn: true
					//event: ['added', 'deleted'],
				},
			},
		}

	});


	// 2. Tell Grunt we plan to use these plug-ins.
	grunt.loadNpmTasks('grunt-inline');
	grunt.loadNpmTasks('grunt-inline-css');
	grunt.loadNpmTasks('grunt-email-builder');	// Does not preserve styles or @media queries. does keep doctype.
	grunt.loadNpmTasks('grunt-contrib-watch');


	// 3. Tell Grunt what to do when we type "grunt" into the terminal.
	grunt.registerTask('default', ['inline','inlinecss']);

};